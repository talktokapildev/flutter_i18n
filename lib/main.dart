
import 'package:flutter/material.dart';
import 'package:flutter\_localizations/flutter\_localizations.dart';

//import 'DemoLocalizations.dart';
//import 'DemoLocalizationsDelegate.dart';

import 'package:flutter_i18n/DemoLocalizations.dart';
import 'package:flutter_i18n/DemoLocalizationsDelegate.dart';


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      supportedLocales: [
        const Locale('tr', 'TR'),
        const Locale('en', 'US')
      ],
      localizationsDelegates: [
        const DemoLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      localeResolutionCallback: (Locale locale, Iterable<Locale> supportedLocales) {
        for (Locale supportedLocale in supportedLocales) {
          if (supportedLocale.languageCode == locale.languageCode || supportedLocale.countryCode == locale.countryCode) {
            return supportedLocale;
          }
        }

        return supportedLocales.first;
      },
      title: 'Flutter Internationalization',
      home: new MyPage(),
    );
  }
}

class MyPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Text(
            DemoLocalizations.of(context).trans("hello_world")
            //Localizations.of<DemoLocalizations>(context, DemoLocalizations).trans("hello_world")

        ),
      ),
    );
  }
}

void main() {
  runApp(new MyApp());
}
